package demo.investment.funds.validator;

import demo.investment.funds.model.FundType;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Arrays;

public interface DecayValidator {

    void validate(ImmutablePair<FundType, Double>... decayPairs);

}
