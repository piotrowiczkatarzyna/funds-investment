package demo.investment.funds.validator;

import demo.investment.funds.model.FundType;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Arrays;

public class DecayValidatorImpl implements DecayValidator {

    public void validate(ImmutablePair<FundType, Double>... decayPairs) {
        validatePercentSum(decayPairs);
    }

    private void validatePercentSum(ImmutablePair<FundType, Double>[] decayPairs) {
        Double percent = Arrays.stream(decayPairs).mapToDouble(pair -> pair.getValue()).sum();
        if(percent.intValue() != 100) {
            throw new IllegalArgumentException("Percentage of all inserted pairs should sum up to 100%");
        }
    }
}
