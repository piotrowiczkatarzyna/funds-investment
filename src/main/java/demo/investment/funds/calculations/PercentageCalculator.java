package demo.investment.funds.calculations;

import demo.investment.funds.model.FundType;

import java.util.Map;

public interface PercentageCalculator {

    Map<FundType, Double> calculateFundPercentContribution(Map<FundType, Integer> fundsDivision, Decay decay);
}
