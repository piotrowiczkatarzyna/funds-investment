package demo.investment.funds.calculations;

import demo.investment.funds.model.FundType;

import java.util.HashMap;
import java.util.Map;

public class BasicPercentageCalculator implements PercentageCalculator {

    private Map<FundType, Double> fundPercentContribusion = new HashMap();

    @Override
    public Map<FundType, Double> calculateFundPercentContribution(Map<FundType, Integer> fundsDivision, Decay decay) {
        fundsDivision.forEach((type, divisionFactor) -> {
            Double factor =  decay.getDecayFactorByFundType(type);
            fundPercentContribusion.put(type, factor);
        });
        return fundPercentContribusion;
    }
}
