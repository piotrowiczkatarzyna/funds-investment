package demo.investment.funds.calculations;

import demo.investment.funds.model.Fund;
import demo.investment.funds.model.FundType;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.List;
import java.util.Map;

public interface ValueCalculator {

    Map<Fund, ImmutablePair<Double, Integer>> calculate(Double amount,
                                                        List<Fund> funds,
                                                        Map<FundType, Double> fundPercentageContribution,
                                                        Map<FundType, Integer> numberOfFundsByType);
}
