package demo.investment.funds.calculations;

import demo.investment.funds.model.Fund;
import demo.investment.funds.model.FundType;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.tuple.ImmutablePair.of;

@AllArgsConstructor
public class ValueCalculatorImpl implements ValueCalculator {

    private Map<FundType, Double> fundPercentageContribution;
    private Map<FundType, Integer> numberOfFundsByType;

    @Override
    public Map<Fund, ImmutablePair<Double, Integer>> calculate(Double amount, List<Fund> funds) {
        Map<Fund, ImmutablePair<Double, Integer>> results = new HashMap<>();
        funds.stream().forEach(fund -> {
            Double percent = getFundContribution(fund.getType()) / getNumberOfFundsByType(fund.getType());
            Integer fundAmount = Double.valueOf(amount * percent / 100).intValue(); //TODO
            results.put(fund, of(percent, fundAmount));
        });
        return results;
    }

    private Double

    private Double getFundContribution(FundType fundType) {
        return fundPercentageContribution.get(fundType);
    }

    private Double getNumberOfFundsByType(FundType fundType) {
        return fundPercentageContribution.get(fundType);
    }
}
