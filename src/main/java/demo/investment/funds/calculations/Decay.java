package demo.investment.funds.calculations;

import demo.investment.funds.model.FundType;
import demo.investment.funds.validator.DecayValidator;
import demo.investment.funds.validator.DecayValidatorImpl;
import lombok.Getter;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Decay {

    private DecayValidator decayValidator = new DecayValidatorImpl();

    @Getter
    private Map<FundType, Double> decayValues;

    public Decay(ImmutablePair<FundType, Double>... decayPairs){
        decayValidator.validate(decayPairs);
        decayValues = new HashMap<>(5);
        Arrays.stream(decayPairs).forEach(pair -> {
            decayValues.put(pair.getKey(), pair.getValue());
        });
    }

    public Double getDecayFactorByFundType(FundType fundType) {
        return decayValues.get(fundType);
    }


}
