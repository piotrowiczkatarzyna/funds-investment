package demo.investment.funds.model;

import demo.investment.funds.calculations.Decay;
import lombok.Getter;

import static org.apache.commons.lang3.tuple.ImmutablePair.of;

@Getter
public enum InvestmentStyle {

    SAFE(new Decay(of(FundType.POLISH, 20.0), of(FundType.FOREIGN, 75.0), of(FundType.CASH, 5.0))),
    BALANCED(new Decay(of(FundType.POLISH, 30.0), of(FundType.FOREIGN, 60.0), of(FundType.CASH, 10.0))),
    AGGRESSIVE(new Decay(of(FundType.POLISH, 40.0), of(FundType.FOREIGN, 20.0), of(FundType.CASH, 40.0)));

    private Decay decay;

    InvestmentStyle(Decay decay){
        this.decay = decay;
    }

}
