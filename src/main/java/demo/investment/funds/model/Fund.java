package demo.investment.funds.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Fund implements Comparable<Fund>{
    private Integer id;
    private FundType type;
    private String name;

    @Override
    public int compareTo(Fund o) {
        return getType().compareTo(o.getType());
    }
}
