package demo.investment.funds.model;

public enum FundType {
    POLISH,
    FOREIGN,
    CASH
}
