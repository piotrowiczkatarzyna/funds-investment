package demo.investment.funds.app;

import demo.investment.funds.calculations.BasicPercentageCalculator;
import demo.investment.funds.calculations.PercentageCalculator;
import demo.investment.funds.calculations.ValueCalculator;
import demo.investment.funds.calculations.ValueCalculatorImpl;
import demo.investment.funds.model.Fund;
import demo.investment.funds.model.FundType;
import demo.investment.funds.model.InvestmentStyle;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.tuple.ImmutablePair.of;


public class BasicCalculationFacadeImp implements CalculationFacade {

    private PercentageCalculator percentageCalculator = new BasicPercentageCalculator();
    private ValueCalculator valueCalculator = new ValueCalculatorImpl();

    private Map<FundType, Integer> numberOfFundsByType = new HashMap();
    private Map<Fund, ImmutablePair<Double, Integer>> eachChosenFundValue = new HashMap();

    @Override
    public void calculate(Double amount, InvestmentStyle style, List<Fund> chosenFunds){

        Collections.sort(chosenFunds);
        countFundsByType(chosenFunds);
        Map<FundType, Double> fundPercentageContribution = percentageCalculator.calculateFundPercentContribution(numberOfFundsByType, style.getDecay());
        valueCalculator.calculate(chosenFunds, fundPercentageContribution, numberOfFundsByType);
        eachChosenFundValue.forEach((fund, pair) -> {
            System.out.printf("Fund: %10s | Type: %10s | Percent: %5.2f | Value: %4d | %n", fund.getName(), fund.getType(), pair.getLeft(), pair.getRight());
        });
    }

    private void countFundsByType(List<Fund> chosenFunds) {
        chosenFunds.stream().forEach(fund -> {
            numberOfFundsByType.merge(fund.getType(), 1, Integer::sum);
        });
    }
}
