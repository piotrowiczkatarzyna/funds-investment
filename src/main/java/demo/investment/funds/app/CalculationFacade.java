package demo.investment.funds.app;

import demo.investment.funds.model.Fund;
import demo.investment.funds.model.InvestmentStyle;

import java.util.List;

public interface CalculationFacade {

    public void calculate(Double amount, InvestmentStyle style, List<Fund> chosenFunds);

}
