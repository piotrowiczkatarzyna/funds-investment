package demo.investment.funds;

import demo.investment.funds.app.BasicCalculationFacadeImp;
import demo.investment.funds.model.Fund;
import demo.investment.funds.model.FundType;
import demo.investment.funds.model.InvestmentStyle;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CalcModuleTest {

    @Test
    public void calcTest(){

        //given
        List<Fund> chosenFunds = new ArrayList<>();
        chosenFunds.add(Fund.builder().id(1).name("Polish1").type(FundType.POLISH).build());
        chosenFunds.add(Fund.builder().id(2).name("Polish2").type(FundType.POLISH).build());
        chosenFunds.add(Fund.builder().id(3).name("Foreign3").type(FundType.FOREIGN).build());
        chosenFunds.add(Fund.builder().id(4).name("Foreign1").type(FundType.FOREIGN).build());
        chosenFunds.add(Fund.builder().id(5).name("Foreign2").type(FundType.FOREIGN).build());
        chosenFunds.add(Fund.builder().id(6).name("Cash1").type(FundType.CASH).build());
        Double amount = Double.valueOf(10001);
        BasicCalculationFacadeImp module = new BasicCalculationFacadeImp();

        //when
        module.calculate(amount, InvestmentStyle.SAFE, chosenFunds);
    }

}